// Verboso
// import { Negociacao } from './../models/Negociacao';
// import { MensagemView } from './../views/MensagemView';
// import { NegociacoesView } from './../views/NegociacoesView';
// import { Negociacoes } from '../models/Negociacoes';

// Sucinto (barrels)
import { NegociacoesView, MensagemView } from '../views/index';
import { Negociacao, Negociacoes } from '../models/index';

export class NegociacaoController {
  private _inputData: HTMLInputElement;
  private _inputQuantidade: HTMLInputElement;
  private _inputValor: HTMLInputElement;

  /**
   * Exemplo de inferência de tipos
   */

  //verboso
  //private _negociacoes: Negociacoes = new Negociacoes();

  //Sucinto
  private _negociacoes = new Negociacoes();

  /**
   * View
   */
  private _negociacoesView = new NegociacoesView('#negociacoesView');
  private _mensagemView = new MensagemView('#mensagemView');

  constructor() {
    /**
     * Mapeamento de inputs com
     * casting explícito
     */
    this._inputData = <HTMLInputElement>document.querySelector('#data');

    this._inputQuantidade = <HTMLInputElement>(
      document.querySelector('#quantidade')
    );

    this._inputValor = <HTMLInputElement>document.querySelector('#valor');

    /**
     * Preenchendo view
     */
    this._negociacoesView.update(this._negociacoes);
  }

  adiciona(evento: Event) {
    console.log(evento);
    evento.preventDefault();

    const negociacao = new Negociacao(
      //this._inputData.value,
      new Date(this._inputData.value.replace(/-/g, ',')),
      parseInt(this._inputQuantidade.value),
      parseFloat(this._inputValor.value)
    );

    this._negociacoes.adiciona(negociacao);
    this._negociacoesView.update(this._negociacoes);
    this._mensagemView.update('Negociação adicionada com sucesso!');
  }
}
