import { Negociacao } from './Negociacao';

export class Negociacoes {
  //Verboso
  //private _negociacoes: Array<Negociacao> = [];

  //Sucinto
  private _negociacoes: Negociacao[] = [];

  adiciona(negociacao: Negociacao): void {
    this._negociacoes.push(negociacao);
  }

  /**
   * O tipo de retorno auxilia em
   * validações e também no
   * Intellisense
   */
  paraArray(): Negociacao[] {
    //Mutável
    //return this._negociacoes;

    //Imutável
    return [].concat(this._negociacoes);
  }
}
