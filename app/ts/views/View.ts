export abstract class View<T> {
  /**
   * Elemento a ser renderizado em
   * index.html
   */
  //private _elemento: Element;
  private _elemento: Element;

  constructor(seletor: string) {
    /**
     * Obtendo elemento do DOM através
     * do seletor
     */
    //this._elemento = document.querySelector(seletor);
    this._elemento = document.querySelector(seletor);
  }

  update(model: T): void {
    console.log(model);
    this._elemento.innerHTML = this.template(model);
  }

  abstract template(model: T): string;
}
